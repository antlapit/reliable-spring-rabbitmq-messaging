package custis.demo.publisher.infra.sender;

import java.util.Map;

public interface MessageSender {
    void send(String destination, String routingKey, byte[] payload, Map<String, String> headers);
}
