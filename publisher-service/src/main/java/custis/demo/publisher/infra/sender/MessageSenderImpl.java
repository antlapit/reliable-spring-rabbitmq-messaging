package custis.demo.publisher.infra.sender;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class MessageSenderImpl implements MessageSender {

    private final RabbitTemplate rabbitTemplate;

    public MessageSenderImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void send(String destination, String routingKey, byte[] payload, Map<String, String> headers) {
        MessageProperties props = new MessageProperties();
        props.setContentType("application/json");
        props.getHeaders().putAll(headers);
        rabbitTemplate.send(destination,
                routingKey,
                new Message(payload, props));
    }
}
