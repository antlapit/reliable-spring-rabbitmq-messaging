package custis.demo.publisher.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {

    @Bean
    public Exchange mainExchange(AmqpAdmin amqpAdmin,
                                 @Value("${events.result.exchange:ResultEvents}") String exchangeName) {
        Exchange exchange = new TopicExchange(exchangeName);
        amqpAdmin.declareExchange(exchange);
        return exchange;
    }
}
