package custis.demo.publisher.api.web;

import custis.demo.publisher.app.ResultsService;
import custis.demo.publisher.domain.Result;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/results")
public class ResultsResource {

    private final ResultsService resultsService;

    public ResultsResource(ResultsService resultsService) {
        this.resultsService = resultsService;
    }

    @PostMapping
    // TODO return dto
    public Result createResult(@RequestBody CreateResultRequest request) {
        return resultsService.modifyStudentResult(request.getStudentId(), request.getCourseId(), request.getValue());
    }

    @GetMapping
    // TODO return dto
    public List<Result> findAllResults(@RequestParam String courseId) {
        return resultsService.findAllResultsByCourseId(courseId);
    }

    @DeleteMapping
    public void deleteAllResults() {
        resultsService.deleteAllResults();
    }

    @Data
    @AllArgsConstructor
    public static final class CreateResultRequest {
        private String studentId;
        private String courseId;
        private int value;
    }
}
