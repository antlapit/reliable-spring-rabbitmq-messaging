package custis.demo.publisher.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import custis.demo.publisher.app.event.ResultUpdatedEventData;
import custis.demo.publisher.domain.Result;
import custis.demo.publisher.domain.ResultsRepository;
import custis.demo.publisher.infra.sender.MessageSender;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Exchange;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ResultsServiceImpl implements ResultsService {

    private final ResultsRepository resultsRepository;
    private final MessageSender messageSender;
    private final Exchange exchange;
    private final ObjectMapper objectMapper;

    public ResultsServiceImpl(ResultsRepository resultsRepository,
                              MessageSender messageSender, Exchange exchange,
                              ObjectMapper objectMapper) {
        this.resultsRepository = resultsRepository;
        this.messageSender = messageSender;
        this.exchange = exchange;
        this.objectMapper = objectMapper;
    }

    @Override
    @Transactional
    public Result modifyStudentResult(String studentId, String courseId, int value) {
        // existing result
        Optional<Result> optResult = resultsRepository.findByStudentIdAndCourseId(studentId, courseId);

        // updating result
        Result newResult = createOrUpdateResult(optResult, studentId, courseId, value);

        // publishing event
        publish(
                ResultUpdatedEventData.builder()
                        .id(newResult.getId())
                        .courseId(newResult.getCourseId())
                        .studentId(newResult.getStudentId())
                        .newValue(newResult.getValue())
                        .oldValue(optResult.map(Result::getValue).orElse(null))
                        .build()
        );

        // do something useful
        return newResult;
    }

    @SneakyThrows
    void publish(ResultUpdatedEventData message) {
        byte[] payload = objectMapper.writeValueAsBytes(message);
        Map<String, String> headers = new HashMap<>();
        headers.put("EventType", "Results.ResultUpdated");
        messageSender.send(exchange.getName(), "event.Results.ResultUpdated", payload, headers);
    }

    @Override
    public List<Result> findAllResultsByCourseId(String courseId) {
        return resultsRepository.findAllByCourseId(courseId);
    }

    @Override
    @Transactional
    public void deleteAllResults() {
        resultsRepository.deleteAll();
    }

    private Result createOrUpdateResult(Optional<Result> existingResult, String studentId, String courseId, int value) {
        Result result;
        if (existingResult.isPresent()) {
            result = existingResult.get();
            result.setValue(value);
        } else {
            result = new Result(
                    UUID.randomUUID().toString(),
                    studentId,
                    courseId,
                    value
            );
        }
        return resultsRepository.save(result);
    }
}
