package custis.demo.publisher.app.event;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultUpdatedEventData {

    private String id;
    private String studentId;
    private String courseId;
    private Integer newValue;
    private Integer oldValue;
}
