package custis.demo.publisher.app;

import custis.demo.publisher.domain.Result;

import java.util.List;

public interface ResultsService {

    Result modifyStudentResult(String studentId, String courseId, int value);

    List<Result> findAllResultsByCourseId(String courseId);

    void deleteAllResults();
}
