package custis.demo.publisher.domain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "result")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Result {

    @Id
    private String id;

    @Column
    private String studentId;

    @Column
    private String courseId;

    @Column
    private int value;

    public Result(String id, String studentId, String courseId, int value) {
        this.id = id;
        this.studentId = studentId;
        this.courseId = courseId;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
}
