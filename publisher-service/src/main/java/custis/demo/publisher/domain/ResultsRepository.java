package custis.demo.publisher.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResultsRepository extends JpaRepository<Result, String> {

    Optional<Result> findByStudentIdAndCourseId(String studentId, String courseId);

    List<Result> findAllByCourseId(String courseId);
}
