package custis.demo.consumer.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRankRepository extends JpaRepository<CourseRank, String> {


    List<CourseRank> findAllByCourseId(String courseId);
}
