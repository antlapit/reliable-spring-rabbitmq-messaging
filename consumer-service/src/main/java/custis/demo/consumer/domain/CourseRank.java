package custis.demo.consumer.domain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "course_rank")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CourseRank {

    @Id
    private String id;

    @Column
    private String studentId;

    @Column
    private String courseId;

    @Column
    private Integer result;

    @Column
    private int rank;

    public CourseRank(String id, String studentId, String courseId, Integer result, int rank) {
        this.id = id;
        this.studentId = studentId;
        this.courseId = courseId;
        this.result = result;
        this.rank = rank;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
}
