package custis.demo.consumer.util.amqp.exception;

public class RequeueException extends RuntimeException {
    public RequeueException(Exception e) {
        super(e);
    }
}
