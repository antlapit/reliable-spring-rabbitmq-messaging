package custis.demo.consumer.util.amqp;

import org.springframework.amqp.support.converter.DefaultClassMapper;

public class AmqpClassMapper extends DefaultClassMapper {

    public static final String EVENT_TYPE_HEADER = "EventType";


    @Override
    public String getClassIdFieldName() {
        return EVENT_TYPE_HEADER;
    }
}
