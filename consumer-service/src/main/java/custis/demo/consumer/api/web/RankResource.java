package custis.demo.consumer.api.web;

import custis.demo.consumer.app.RankService;
import custis.demo.consumer.domain.CourseRank;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/course-ranks")
public class RankResource {

    private final RankService rankService;

    public RankResource(RankService rankService) {
        this.rankService = rankService;
    }

    @GetMapping
    // TODO return dto
    public List<CourseRank> findAllRanks(@RequestParam String courseId) {
        return rankService.findAllRanksByCourseId(courseId);
    }

    @DeleteMapping
    public void deleteAllRanks() {
        rankService.deleteAllRanks();
    }
}
