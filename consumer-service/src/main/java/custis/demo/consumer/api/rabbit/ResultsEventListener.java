package custis.demo.consumer.api.rabbit;

import custis.demo.consumer.app.RankService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@RabbitListener(
        id = "results-event-listener",
        queues = "#{resultsQueue.name}"
)
@Slf4j
public class ResultsEventListener {

    private final RankService rankService;

    @Autowired
    public ResultsEventListener(RankService rankService) {
        this.rankService = rankService;
    }

    @RabbitHandler
    public void handle(@Payload ResultUpdatedMessage message) {
        rankService.recalculateCourseRank(message.getCourseId(),
                message.getStudentId(),
                message.getNewValue(),
                message.getOldValue());
    }
}
