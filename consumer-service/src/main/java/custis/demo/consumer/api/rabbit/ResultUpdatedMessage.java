package custis.demo.consumer.api.rabbit;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultUpdatedMessage {

    private String id;
    private String studentId;
    private String courseId;
    private Integer newValue;
    private Integer oldValue;
}
