package custis.demo.consumer.app;

import custis.demo.consumer.domain.CourseRank;

import java.util.List;

public interface RankService {

    void recalculateCourseRank(String courseId, String studentId, Integer newValue, Integer oldValue);

    List<CourseRank> findAllRanksByCourseId(String courseId);

    void deleteAllRanks();
}
