package custis.demo.consumer.app;

import custis.demo.consumer.domain.CourseRank;
import custis.demo.consumer.domain.CourseRankRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RankServiceImpl implements RankService {

    private final CourseRankRepository courseRankRepository;

    public RankServiceImpl(CourseRankRepository courseRankRepository) {
        this.courseRankRepository = courseRankRepository;
    }

    @Override
    @Transactional
    public void recalculateCourseRank(String courseId, String studentId, Integer newValue, Integer oldValue) {
        List<CourseRank> ranks = courseRankRepository.findAllByCourseId(courseId);
        Optional<CourseRank> studentRank = ranks.stream()
                .filter(courseRank -> courseRank.getCourseId().equals(courseId))
                .filter(courseRank -> courseRank.getStudentId().equals(studentId))
                .findAny();

        List<CourseRank> updatedRanks = new ArrayList<>();
        if (studentRank.isPresent()) {
            CourseRank existingRank = studentRank.get();
            existingRank.setResult(newValue);
            updatedRanks.add(existingRank);
        } else {
            updatedRanks.add(
                    new CourseRank(
                            UUID.randomUUID().toString(),
                            studentId,
                            courseId,
                            newValue,
                            0
                    )
            );
        }
        courseRankRepository.saveAll(updatedRanks);
    }

    @Override
    public List<CourseRank> findAllRanksByCourseId(String courseId) {
        return courseRankRepository.findAllByCourseId(courseId);
    }

    @Override
    @Transactional
    public void deleteAllRanks() {
        courseRankRepository.deleteAll();
    }
}
