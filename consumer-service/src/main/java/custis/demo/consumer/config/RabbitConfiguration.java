package custis.demo.consumer.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import custis.demo.consumer.api.rabbit.ResultUpdatedMessage;
import custis.demo.consumer.util.amqp.AmqpClassMapper;
import custis.demo.consumer.util.amqp.AmqpErrorHandler;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ErrorHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

@Configuration
public class RabbitConfiguration {

    @Bean
    public DefaultClassMapper typeMapper() {
        Map<String, Class<?>> idClassMapping = new HashMap<>();

        idClassMapping.put("Results.ResultUpdated", ResultUpdatedMessage.class);

        AmqpClassMapper typeMapper = new AmqpClassMapper();
        typeMapper.setIdClassMapping(idClassMapping);
        return typeMapper;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
            final SimpleRabbitListenerContainerFactoryConfigurer configurer,
            final ConnectionFactory connectionFactory,
            @Value("${rabbitmq.prefetch-count:10}") int prefetchCount,
            @Qualifier("rabbitExecutor") Executor asyncExecutor) {

        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory() {
            @Override
            protected SimpleMessageListenerContainer createContainerInstance() {
                SimpleMessageListenerContainer container = super.createContainerInstance();
                // если очередь exclusive, то несколько сервисов не могут получить к ней доступ одновременно
                // это вполне нормальная ситуация, поэтому такое логирование выполняется с уровнем DEBUG
                container.setExclusiveConsumerExceptionLogger((log, s, throwable) -> log.debug(s));
                // устанавливаем
                container.setPrefetchCount(prefetchCount);
                return container;
            }
        };
        configurer.configure(factory, connectionFactory);
        factory.setErrorHandler(amqpErrorHandler());
        factory.setTaskExecutor(asyncExecutor);
        return factory;
    }

    @Bean
    public ErrorHandler amqpErrorHandler() {
        return new AmqpErrorHandler();
    }

    @Bean
    public MessageConverter messageConverter(ObjectMapper objectMapper) {
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter(objectMapper);
        messageConverter.setClassMapper(typeMapper());
        return messageConverter;
    }

    @Bean
    public Queue resultsQueue(AmqpAdmin amqpAdmin,
                              @Value("${events.result.exchange:ResultEvents}") String exchangeName,
                              @Value("${events.result.queue:result-events-q}") String queueName) {
        Queue queue = new Queue(queueName);
        amqpAdmin.declareQueue(queue);

        amqpAdmin.declareBinding(
                new Binding(
                        queueName,
                        Binding.DestinationType.QUEUE,
                        exchangeName,
                        "event.Results.ResultUpdated",
                        null
                )
        );
        return queue;
    }
}
