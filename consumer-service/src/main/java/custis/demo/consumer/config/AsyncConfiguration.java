package custis.demo.consumer.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class AsyncConfiguration {
    @Bean
    @Qualifier("rabbitExecutor")
    public Executor rabbitExecutor(@Value("${async.listener-size:10}") int listenerSize) {
        //SimpleListenerContainer не умеет работать с ThreadPool  - только с Async
        SimpleAsyncTaskExecutor executor = new SimpleAsyncTaskExecutor();
        executor.setThreadNamePrefix("Rabbit-");
        executor.setConcurrencyLimit(listenerSize);
        return executor;
    }
}
